from django.forms.widgets import Input


class PasswordShowToggleWidget(Input):
    input_type = 'password'
    template_name = 'widgets/toggle_password_widget.html'

    class Media:
        js = ('js/widget.js', )
