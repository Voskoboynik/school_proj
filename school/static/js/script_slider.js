$(document).ready(function () {
    let groups = $('.group-set').children();
    let group = groups.first().show();
    let slider = $('div.slider-show');

    let nextSlides = function(){
        group.animate({width: 'toggle'}, 200);
        group = group.next().animate({width: 'toggle'}, 100);
            if (group.next().length) {
                nextSlides();
            }
    };

    let prevSlides = function () {
        group.animate({width: 'toggle'}, 200);
        group = group.prev().animate({width: 'toggle'}, 100);
            if (group.prev().length) {
                prevSlides();
            }
    };

    slider.on('click', '.group-next', function () {
        if (!group.next().length) {
            prevSlides()
        } else {
            group.animate({width: 'toggle'}, 300);
            group = group.next().animate({width: 'toggle'}, 300);
        }
    });

    slider.on('click', '.group-prev', function () {
        if (!group.prev().length) {
            nextSlides()
        } else {
            group.animate({width: 'toggle'}, 300);
            group = group.prev().animate({width: 'toggle'}, 300);
        }
    });
});

